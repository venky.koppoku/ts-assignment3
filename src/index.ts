export const map = <T, S>(func: (arg: T) => S) => (arr: T[]): S[] => {
  const result: S[] = [];
  for (const element of arr) {
    result.push(func(element));
  }
  return result;
};

export const filter = <T>(func: (arg: T) => boolean) => (arr: T[]): T[] => {
  const result: T[] = [];
  for (const element of arr) {
    if (func(element)) {
      result.push(element);
    }
  }
  return result;
};

export const reduce = <T, S>(func: (arg: T, a: S) => S) => (
  acc: S,
  arr: T[]
): S => {
  let res1: S = acc;
  for (const element of arr) {
    res1 = func(element, res1);
  }
  return res1;
};

export function max<T>(arr: T[]) {
  if (arr.length === 0) {
    throw Error('array must contain one or more elements');
  }
  return reduce((x, y) => (x > y ? x : y))(arr[0], arr);
}

export function unique<T>(arr: T[]) {
  return reduce<T, T[]>(
    (ele, acc) => (acc.includes(ele) ? acc : [...acc, ele])
  )([], arr);
}
