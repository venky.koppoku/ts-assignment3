import { map, filter, reduce, max, unique } from './index';

describe('function map<T, U>', () => {
  it('should return [] if input is []', () => {
    expect(map<number, number>(x => x * 2)([])).toEqual([]);
  });

  it('should return [1,4,9] if input is [1,2,3]', () => {
    expect(map<number, number>(x => x * x)([1, 2, 3])).toEqual([1, 4, 9]);
  });

  it('should return [3,6,9] if input is [1,2,3]', () => {
    expect(map<number, number>(x => x * 3)([1, 2, 3])).toEqual([3, 6, 9]);
  });
});

describe('function filter<T>', () => {
  it('should return [] if input is []', () => {
    expect(filter<number>(x => x > 2)([])).toEqual([]);
  });

  it('should return [3,4,5] if input is [1,2,3,4,5]', () => {
    expect(filter<number>(x => x > 2)([1, 2, 3, 4, 5])).toEqual([3, 4, 5]);
  });
});

describe('function reduce<T>', () => {
  it('should return 8', () => {
    const add = (x: number, y: number): number => x + y;
    expect(reduce(add)(0, [1, 2, 3])).toBe(6);
  });
});

describe('function max<T>', () => {
  it('max of [1] should be 1', () => {
    expect(max([1])).toEqual(1);
  });
  it('max of [1,2,3] should be 3', () => {
    expect(max([1, 2, 3])).toEqual(3);
  });

  it('max of [-1,-2,-3] should be -1', () => {
    expect(max([-1, -2, -3])).toEqual(-1);
  });
});

describe('function unique<T>', () => {
  it('unique elements  of [1,1,2] should be [1,2,]', () => {
    expect(unique([1, 1, 2])).toEqual([1, 2]);
  });
  it('unique elements  of [1,1] should be [1]', () => {
    expect(unique([1, 1])).toEqual([1]);
  });
});
